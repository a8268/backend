CREATE TABLE sensor_measurement (
    measure_id uuid,
    sensor_name VARCHAR(100) NOT NULL,
    sensor_measure NUMERIC (8, 2) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (measure_id)
);