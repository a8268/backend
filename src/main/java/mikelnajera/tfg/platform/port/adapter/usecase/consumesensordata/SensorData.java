package mikelnajera.tfg.platform.port.adapter.usecase.consumesensordata;

import java.math.BigDecimal;

public class SensorData {

    private String name;
    private BigDecimal value;

    public SensorData() {

    }

    public SensorData(String name, BigDecimal value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
