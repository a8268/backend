package mikelnajera.tfg.platform.port.adapter.usecase.consumesensordata;

import io.vertx.core.json.Json;
import io.vertx.core.buffer.Buffer;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ConsumeSensorDataMQTTAdapter implements ConsumeSensorDataPort {

    @Inject
    EventBus eventBus;

    @Incoming("sensors")
    public void process(byte[] data) {
        SensorData sensorData  = Json.decodeValue(Buffer.buffer(data), SensorData.class);
        this.eventBus.publish("sensors", sensorData);
    }

}
