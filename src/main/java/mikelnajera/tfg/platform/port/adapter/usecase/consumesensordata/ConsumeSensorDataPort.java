package mikelnajera.tfg.platform.port.adapter.usecase.consumesensordata;

public interface ConsumeSensorDataPort {

    void process(byte[] data);

}
