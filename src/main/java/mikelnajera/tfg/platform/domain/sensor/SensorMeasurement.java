package mikelnajera.tfg.platform.domain.sensor;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity(name = "sensor_measurement")
public class SensorMeasurement extends PanacheEntityBase {

    @Id
    @GeneratedValue
    @Column(name = "measure_id")
    private UUID id;

    @Column(name = "sensor_name")
    private String sensorName;

    @Column(name = "sensor_measure")
    private BigDecimal measure;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public SensorMeasurement() {}

    public SensorMeasurement(String sensorName, BigDecimal measure, LocalDateTime createdAt) {
        this.sensorName = sensorName;
        this.measure = measure;
        this.createdAt = createdAt;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public BigDecimal getMeasure() {
        return measure;
    }

    public void setMeasure(BigDecimal measure) {
        this.measure = measure;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "SensorMeasurement{" +
                "id=" + id +
                ", sensorName='" + sensorName + '\'' +
                ", measure=" + measure +
                ", createdAt=" + createdAt +
                '}';
    }

}
