package mikelnajera.tfg.platform.domain.sensor;

import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.common.annotation.Blocking;
import mikelnajera.tfg.platform.port.adapter.usecase.consumesensordata.SensorData;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@ApplicationScoped
public class SensorMeasurementDomainService {

    @Inject SensorMeasurementRepository sensorMeasurementRepository;

    public List<SensorMeasurement> findBySensorName(String sensorName) {
        return this.sensorMeasurementRepository.list("sensorName", sensorName);
    }

    @Transactional
    public void persist(SensorMeasurement sensorMeasurement) {
        this.sensorMeasurementRepository.persist(sensorMeasurement);
    }

    @ConsumeEvent(value = "sensors")
    @Blocking
    public void consumeSensorMessage(SensorData sensorData) {
        SensorMeasurement sensorMeasurement = new SensorMeasurement(
            sensorData.getName(),
            sensorData.getValue(),
            LocalDateTime.now()
        );

        this.persist(sensorMeasurement);
    }

}
