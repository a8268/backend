package mikelnajera.tfg.platform.domain.sensor;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SensorMeasurementRepository implements PanacheRepository<SensorMeasurement> {

}
