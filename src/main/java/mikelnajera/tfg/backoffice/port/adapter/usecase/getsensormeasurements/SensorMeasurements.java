package mikelnajera.tfg.backoffice.port.adapter.usecase.getsensormeasurements;

import java.util.List;

public class SensorMeasurements {

    private String sensorName;
    private List<Measurement> measurements;

    public SensorMeasurements() {

    }

    public SensorMeasurements(String sensorName, List<Measurement> measurements) {
        this.sensorName = sensorName;
        this.measurements = measurements;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public List<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<Measurement> measurements) {
        this.measurements = measurements;
    }

}
