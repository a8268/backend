package mikelnajera.tfg.backoffice.port.adapter.usecase.getsensormeasurements;

import mikelnajera.tfg.platform.domain.sensor.SensorMeasurement;
import mikelnajera.tfg.platform.domain.sensor.SensorMeasurementDomainService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponseSchema;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "sensors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/sensors")
public class GetSensorMeasurementsHttpAdapter implements GetSensorMeasurementsPort {

    @Inject SensorMeasurementDomainService sensorMeasurementDomainService;

    @GET
    @Path("/{sensorName}")
    @APIResponse(
        responseCode = "500",
        description = "Internal Server error",
        content = @Content(mediaType = "application/json"))
    @APIResponseSchema(value = SensorMeasurements.class,
        responseDescription = "The sensor measurements",
        responseCode = "200")
    @Operation(
        summary = "Get the sensor measurements of a sensor",
        description = "Get the sensor measurements of a sensor requested by its name")
    @Override
    public SensorMeasurements getMeasurementsBySensorName(
            @Parameter(
                description = "The sensor name",
                required = true,
                example = "temperature_sensor",
                schema = @Schema(type = SchemaType.STRING))
            @PathParam("sensorName") String sensorName) {
        SensorMeasurements result = new SensorMeasurements();
        result.setSensorName(sensorName);

        List<SensorMeasurement> results = sensorMeasurementDomainService.findBySensorName(sensorName);
        List<Measurement> measurements = results
                .stream()
                .map(element -> new Measurement(element.getMeasure(), element.getCreatedAt()))
                .collect(Collectors.toList());

        result.setMeasurements(measurements);
        return result;
    }
}
