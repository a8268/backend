package mikelnajera.tfg.backoffice.port.adapter.usecase.getsensormeasurements;

public interface GetSensorMeasurementsPort {

    SensorMeasurements getMeasurementsBySensorName(String sensorName);

}
