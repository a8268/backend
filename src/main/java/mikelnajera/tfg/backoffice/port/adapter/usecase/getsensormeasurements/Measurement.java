package mikelnajera.tfg.backoffice.port.adapter.usecase.getsensormeasurements;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Measurement {

    private BigDecimal value;
    private LocalDateTime createdAt;

    public Measurement() {

    }

    public Measurement(BigDecimal value, LocalDateTime createdAt) {
        this.value = value;
        this.createdAt = createdAt;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

}
