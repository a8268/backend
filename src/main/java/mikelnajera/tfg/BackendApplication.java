package mikelnajera.tfg;

import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@OpenAPIDefinition(
    tags = {
        @Tag(name="sensors", description="Sensors operations.")
    },
    info = @Info(
        title="Agriculture Monitoring API",
        version = "1.0.0",
        contact = @Contact(
            name = "Agriculture Monitoring API Soporte",
            url = "http://exampleurl.com/contact",
            email = "tfgmikelnajera@gmail.com"
        ),
        license = @License(
            name = "Apache 2.0",
            url = "https://www.apache.org/licenses/LICENSE-2.0.html"
        )
    )
)
public class BackendApplication extends Application {
  
}
